package Components;

public enum StepType {
    BAKING,
    MIXING,
    MIXING_AND_BAKING;
}
