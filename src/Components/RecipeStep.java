package Components;

import java.util.ArrayList;
import java.util.List;

public class RecipeStep {
    private List<Ingredient> additivesList;
    private int temperature;
    private int time;
    private float mixingSpeed;
    private StepType stepType;
    private List<Integer> quantitiesList;
    public RecipeStep(List<Ingredient> additivesList,
                      int temperature,
                      int time,
                      float mixingSpeed,
                      StepType stepType,
                      List<Integer> quantitiesList) {
        this.additivesList = additivesList;
        this.temperature = temperature;
        this.time = time;
        this.mixingSpeed = mixingSpeed;
        this.stepType = stepType;
        this.quantitiesList = quantitiesList;
    }

    public RecipeStep(int temperature,
                      int time,
                      float mixingSpeed,
                      StepType stepType) {
        this.temperature = temperature;
        this.time = time;
        this.mixingSpeed = mixingSpeed;
        this.stepType = stepType;
        this.additivesList = new ArrayList<>();
        this.quantitiesList = new ArrayList<>();
    }

    public List<Ingredient> getAdditivesList() {
        return additivesList;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getTime() {
        return time;
    }

    public float getMixingSpeed() {
        return mixingSpeed;
    }

    public StepType getStepType() {
        return stepType;
    }

    public List<Integer> getQuantitiesList() {
        return quantitiesList;
    }
}
