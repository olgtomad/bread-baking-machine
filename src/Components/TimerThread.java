package Components;

public class TimerThread extends Thread {

    private Element element;
    private TemperatureSensor temperatureSensor;
    private int targetTemperature;

    public TimerThread(Element element,
                       TemperatureSensor temperatureSensor,
                       int targetTemperature){
        super();
        this.element = element;
        this.temperatureSensor = temperatureSensor;
        this.targetTemperature = targetTemperature;
    }

    public TimerThread (Element element){
        super();
        this.element = element;
    }

    @Override
    public void run() {
        int heatingTimer = 3;
        element.setRunning(true);
        int chunk = 0;
        if (temperatureSensor != null){
            int currentTemperature = temperatureSensor.getTemp();
            int diff = targetTemperature - currentTemperature;
            chunk = diff/heatingTimer;
        }
        while (element.getTimer()>0){
            if (heatingTimer>0 && element instanceof Heater) {
                temperatureSensor.setTemperature(temperatureSensor.getTemp()+chunk);
                System.out.println("Current temperature: "+temperatureSensor.getTemp());
                heatingTimer--;
                if (heatingTimer==0){
                    System.out.println("Heating finished.");
                    temperatureSensor.setTemperature(targetTemperature);
                }
            }
            else {
                System.out.println("Time remaining: " + element.getTimer());
                element.setTimer(element.getTimer()-1);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        element.setRunning(false);

        if (element instanceof Mixer){
            System.out.println("Mixing finished.");
        } else if (element instanceof Heater) {
            System.out.println("Baking finished.");
        }

    }
}
