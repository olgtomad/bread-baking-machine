package Components;

import java.util.List;

public class Recipe {
    private List<Ingredient> ingredients;
    private List<Integer> quantities;
    private String name;
    private List<RecipeStep> recipeSteps;

    public Recipe(String name,
                  List<Ingredient> ingredients,
                  List<Integer> quantities,
                  List<RecipeStep> recipeSteps) {
        this.name = name;
        this.ingredients = ingredients;
        this.quantities = quantities;
        this.recipeSteps = recipeSteps;
    }

    public Ingredient getIngredient(int index) {

        return ingredients.get(index);
    }

    public int getQuantity(int index) {

        return quantities.get(index);
    }

    public int getIngredientSize(){
        return ingredients.size();
    }

    public String getName() {
        return name;
    }
    public RecipeStep getRecipeStep(int index) {
        return recipeSteps.get(index);
    }

    public int getRecipeStepSize(){
        return recipeSteps.size();
    }
}
