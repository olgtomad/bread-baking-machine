package Components;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ControlPanel {
    private Heater heater;
    private Mixer mixer;
    private WeightSensor weightSensor;
    private TemperatureSensor temperatureSensor;
    private AdditionsFlap additionsFlap;
    private StopButton stopButton;
    private Recipes recipes;

    public ControlPanel(Heater heater,
                        Mixer mixer,
                        WeightSensor weightSensor,
                        TemperatureSensor temperatureSensor,
                        AdditionsFlap additionsFlap,
                        StopButton stopButton,
                        Recipes recipes) {
        this.heater = heater;
        this.mixer = mixer;
        this.weightSensor = weightSensor;
        this.temperatureSensor = temperatureSensor;
        this.additionsFlap = additionsFlap;
        this.stopButton = stopButton;
        this.recipes = recipes;
    }

    public void startHeater(int temperature, int time) {
        System.out.println("Control panel: heater started.");
        heater.setTimer(time);
        heater.setTemp(temperature);
        TimerThread timerThread = new TimerThread(heater, temperatureSensor, temperature);
        timerThread.start();
        try {
            timerThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void startMixer(float speed, int time) {
        System.out.println("Control panel: mixer started.");
        mixer.setSpeed(speed);
        mixer.setTimer(time);
        TimerThread timerThread = new TimerThread(mixer);
        timerThread.start();
        try {
            timerThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void executeStep(int recipeId, int step) {
        System.out.println("Starting step "+ (step+1));
        RecipeStep recipeStep = recipes.getRecipe(recipeId).getRecipeStep(step);

        List<Ingredient> additivesList = recipeStep.getAdditivesList();
        if (!additivesList.isEmpty()) System.out.println("Adding ingredients");
        for (int i = 0; i<additivesList.size(); i++) {
            System.out.println(additivesList.get(i).getName() + " " + recipeStep.getQuantitiesList().get(i));
        }
        System.out.println("Time: "+recipeStep.getTime());
        switch (recipeStep.getStepType()){
            case BAKING -> {
                System.out.println("Temperature: "+recipeStep.getTemperature());
                startHeater(recipeStep.getTemperature(), recipeStep.getTime());
            }

            case MIXING -> {
                System.out.println("Mixing speed: "+recipeStep.getMixingSpeed());
                startMixer(recipeStep.getMixingSpeed(), recipeStep.getTime());
            }

            default -> throw new IllegalStateException("Unexpected recipe step type.");
        }
        System.out.println("Step finished.");
    }

    public void executeRecipe(int recipeId) {
        int listSize = recipes.getRecipe(recipeId).getRecipeStepSize();
        for (int i=0; i<listSize; i++){
            executeStep(recipeId,i);
        }
        System.out.println("Recipe finished.");
    }
    public void menuLoop() {
        boolean inLoop = true;
        while (inLoop){
            System.out.println("Select option:");
            System.out.println("1. Select recipe.");
            System.out.println("2. Exit.");
            Scanner scanner = new Scanner(System.in);
            int option = 0;
            try {
                option = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid option.");
                continue;
            }
            switch (option){
                case 1:
                    selectRecipe();
                    break;

                case 2:
                    inLoop = false;
                    break;

                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }
    }
    private void selectRecipe() {
        int size = recipes.getSize();
        System.out.println("Select recipe:");
        for (int i = 0; i<size; i++){
            System.out.printf("%d: %s%n",i+1, recipes.getRecipe(i).getName());
        }

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt()-1;
        if (option>=0 && option<size) {
            System.out.println("Ingredients:");
            Recipe selectedRecipe = recipes.getRecipe(option);
            for (int i = 0; i<selectedRecipe.getIngredientSize(); i++) {
                System.out.println(selectedRecipe.getIngredient(i).getName()+" "+selectedRecipe.getQuantity(i));
            }
            executeRecipe(option);
        }
        else {
            System.out.println("Invalid option.");
        }
    }
}
