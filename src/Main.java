import Components.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("Let's start baking!");
        Ingredient ingredient1 = new Ingredient("Wheat flour");
        Ingredient ingredient2 = new Ingredient("Water");
        Ingredient ingredient3 = new Ingredient("Whole flour");
        Ingredient ingredient4 = new Ingredient("Addition - sesame seeds");
        List<Ingredient> ingredientList = new ArrayList<>();
        ingredientList.add(ingredient1);
        ingredientList.add(ingredient2);
        List<Integer> quantities = new ArrayList<>();
        quantities.add(200);
        quantities.add(50);
        List<Ingredient> additivesList = new ArrayList<>();
        additivesList.add(ingredient2);
        List<Integer> additivesQuantity = new ArrayList<>();
        additivesQuantity.add(100);

        List<RecipeStep> recipeSteps = new ArrayList<>();
        RecipeStep recipeStep = new RecipeStep(0,5,0.8f,StepType.MIXING);

        recipeSteps.add(recipeStep);
        recipeStep = new RecipeStep(additivesList,0,5,1f,StepType.MIXING,additivesQuantity);
        recipeSteps.add(recipeStep);
        recipeStep = new RecipeStep(500,8,0,StepType.BAKING);
        recipeSteps.add(recipeStep);

        Recipe recipe = new Recipe("Plain bread.", ingredientList, quantities, recipeSteps);


        List<Ingredient> ingredientList2 = new ArrayList<>();
        ingredientList2.add(ingredient3);
        ingredientList2.add(ingredient2);
        ingredientList2.add(ingredient4);
        List<Integer> quantities2 = new ArrayList<>();
        quantities2.add(300);
        quantities2.add(57);
        quantities2.add(2);
        List<Ingredient> additivesList2 = new ArrayList<>();
        additivesList2.add(ingredient3);
        List<Integer> additivesQuantity2 = new ArrayList<>();
        additivesQuantity2.add(10);

        List<RecipeStep> recipeSteps2 = new ArrayList<>();
        RecipeStep recipeStep2 = new RecipeStep(0,5,0.2f,StepType.MIXING);

        recipeSteps2.add(recipeStep2);
        recipeStep2 = new RecipeStep(additivesList2,0,5,0.7f,StepType.MIXING,additivesQuantity);
        recipeSteps2.add(recipeStep2);
        recipeStep2 = new RecipeStep(200,4,0,StepType.BAKING);
        recipeSteps2.add(recipeStep2);
        recipeStep2 = new RecipeStep(270,8,0,StepType.BAKING);
        recipeSteps2.add(recipeStep2);


        Recipe recipe2 = new Recipe("Whole flour sesame bread.", ingredientList2, quantities2, recipeSteps2);
        Recipes recipes = new Recipes();
        recipes.addRecipe(recipe);
        recipes.addRecipe(recipe2);

        Heater heater=new Heater();
        Mixer mixer= new Mixer();
        WeightSensor weightSensor = new WeightSensor();
        TemperatureSensor temperatureSensor = new TemperatureSensor();
        AdditionsFlap additionsFlap = new AdditionsFlap();
        StopButton stopButton = new StopButton();
        ControlPanel controlPanel = new ControlPanel(heater, mixer, weightSensor, temperatureSensor, additionsFlap, stopButton, recipes);
        controlPanel.menuLoop();
    }
}